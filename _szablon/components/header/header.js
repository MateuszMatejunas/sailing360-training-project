/* eslint-env browser */
import $ from 'jquery';

const Header = {
  init() {
    this.catchDOM();
    if (this.$el.length > 0 && this.$box.length > 0) {
      this.catchDOM();
      this.someFunction();
    }
  },
  catchDOM() {
    this.$el = $('.header');
    this.$element = this.$el.find('.header__element');
  },
  someFunction() {}
};

export default Header;
