/* eslint-env browser */
import $ from 'jquery';
import '../utils/scripts/anchor-animate-scroll';

import Header from './header/header';

$(document).ready(() => {
  Header.init();
});
